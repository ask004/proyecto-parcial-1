﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour {

    private Animator Anim;


	// Use this for initialization
	void Start () {
        Anim = GetComponent<Animator>();
	}

    public void StartAnim()
    {
        Anim.Play("MoveCamera1");
    }

    public void ReturnAnim()
    {
        Anim.Play("MoveCamera2");
    }
}
